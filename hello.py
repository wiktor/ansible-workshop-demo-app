from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask import request
import os

app = Flask(__name__)


try:
    import config as cf
    app.config['SQLALCHEMY_DATABASE_URI'] = cf.SQLALCHEMY_DATABASE_URI
except (ImportError, AttributeError):
    @app.route('/')
    def hello_world():
        return u'Hello World! %s' % os.getpid()
else:
    db = SQLAlchemy(app)

    class User(db.Model):
        id = db.Column(db.Integer, primary_key=True)
        username = db.Column(db.String(80), unique=True)
        email = db.Column(db.String(120), unique=True)

        def __init__(self, username, email):
            self.username = username
            self.email = email

        def __repr__(self):
            return '<User %r>' % self.username

    db.create_all()

    def get_mac():
        from uuid import getnode as get_mac
        mac = get_mac()
        txt="%012X" % mac
        return ':'.join(a+b for a,b in zip(txt[::2], txt[1::2]))

    @app.route('/', methods=['GET', 'POST'])
    def hello_world():
        return u'PID IS: %s' % os.getpid()

    @app.route('/<int:obj_id>', methods=['GET'])
    def get_object(obj_id):
        return 'Get object: ' + str(obj_id)


if __name__ == '__main__':
    app.run()